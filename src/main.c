/* Copyright © 2020-2024 Linus Vanas <linus@vanas.fi>
 * SPDX-License-Identifier: MIT
 */
#include <string.h>

#include "../build/config.h"
#include "common.h"

/**
 * Parse arguments and proceed to SDL initialization.
 * @param argc The amount of arguments supplied to the program.
 * @param argv The arguments supplied to the program.
 * @return 0 if the program was ran without errors, a non-zero integer otherwise.
 */
int main(int argc, char **argv)
{
	char print_version = 0;
	char print_help = 0;
	char print_settings = 0;

	for (int i = 1; i < argc; i++)
	{
		if(!strcmp("--version", argv[i]))
		{
			print_version = 1;
			continue;
		}

		if (!strcmp("--help", argv[i]))
		{
			print_help = 1;
			continue;
		}

		if (!strcmp("--help-settings", argv[i]))
		{
			print_settings = 1;
			continue;
		}

		if (!strcmp("--mute", argv[i]))
		{
			Settings.flags.mute = 1;
			continue;
		}

		if (!strcmp("--quiet", argv[i]))
		{
			Settings.flags.quiet = 1;
			continue;
		}

		if (!strcmp("--mute", argv[i]))
		{
			Settings.flags.mute = 1;
			continue;
		}

		if (!strcmp("--hide-counters", argv[i]))
		{
			Settings.flags.hide_counters = 1;
			continue;
		}

		if (!strcmp("--disable-config-file", argv[i]))
		{
			Settings.flags.disable_config_file = 1;
			continue;
		}

		int len = strlen(argv[i]);
		char setting[255], value[len];
		if (sscanf(argv[i], "--%255[^=]=%s", setting, value) == 2)
		{
			set_setting(setting, value);
			continue;
		}

		fprintf(stderr, "Unrecognized option: %s\n", argv[i]);
	}

	if (print_version)
	{
		printf("Parallel_Overhead %s\n", VERSION);
	}

	if (print_help)
	{
		printf("An endless runner game.\n");
		printf("Keep the ships from falling into the holes in the hyperspace tunnel by steering\n");
		printf("them with the left and right arrow keys. Start the game by pressing space.\n");
		printf("Usage: %s [options]\n", argv[0]);
		printf("Options:\n");
		printf(" --help                 Print this help and exit.\n");
		printf(" --version              Print the version number and exit.\n");
		printf(" --quiet                Do not print results into standard output.\n");
		printf(" --mute                 Do not play any audio.\n");
		printf(" --hide-counters        Do not display time and distance counters.\n");
		printf(" --disable-config-file  Do not read from or write to the config file.\n");
		printf(" --<setting>=<value>    Set <setting> to <value> (see below).\n");
		printf(" --help-settings        List available settings (see above) and exit.\n");

	}




	if (print_settings)
	{
		printf
		(
			"Available settings:\n"
			"(These aren't a priority - there are many bugs and values aren't validated.)\n\n"

			"HUD:\n"
			" hud-sector - Which sector of the wall to render the time and distance on.\n"
			" hud-deep - How many tunnel segments in to render the time and distance.\n"
			" hud-alignment - Where to align the numbers in the hud: left = 0 ... 1 = right.\n\n"

			"Frame/tick rate:\n"
			" min-tick-time - How many milliseconds should each game tick take at a minimum.\n"
			" max-tick-time - How many milliseconds should each game tick take at a maximum.\n\n"

			"Seed:\n"
			" seed - Which integer seed to use for generating the level.\n"
			"  Unless 0, level generation is reset for each run.\n"
			"  When 0, a random seed is chosen (default).\n\n"

			"Initial difficulty:\n"
			" initial-speed - Multiplier for the runtime-based base time.\n"
			"  Note that the default is just 0.000075.\n"
			" initial-safe-paths - How many safe paths are generated.\n"
			" initial-safe-transition - For how many tiles rotated safe paths are duplicated.\n"
			" initial-safe-chance - Probability that a tile not on a safe path is safe.\n\n"

			"Difficulty increase on losing a ship:\n"
			"(Each is added to the matching initial setting when a ship is lost.)\n"
			" death-speed-increase\n"
			" death-safe-paths-increase\n"
			" death-safe-transition-increase\n"
			" death-safe-chance-increase\n\n"

			"Ships:\n"
			" ships - How many ships the game starts with.\n"
			" ship-depth - How many segments inwards from the camera the ships are.\n"
			" ship-sector-offset - Offset to the starting sectors of the ships.\n\n"

			"Tunnel:\n"
			" tunnel-sectors - How many sectors the tunnel wall consists of.\n"
			" tunnel-length - How many tunnel segments are visible at a time.\n"
			" tunnel-segment-length - How many tunnel segments are visible at a time.\n"
			" min-color-transition - How many segments a color transition takes at a minimum.\n"
			" max-color-transition - How many segments a color transition takes at a maximum.\n"
		);
	}

	if (print_version || print_help || print_settings)
	{
		return 0;
	}

	return init_SDL();
}
