/* Copyright © 2020-2024 Linus Vanas <linus@vanas.fi>
 * SPDX-License-Identifier: MIT
 */
#include <math.h>
#include "common.h"

//Global struct for holding the number graphics and data.
struct
{
	SDL_Surface *surface;
	SDL_Rect rects[12];
}
HUD;

/**
 * Render the given number characters to the given surface.
 * @param target The surface to render to.
 * @param target_rect The area of the surface in which to render.
 * @param numc The amount of number characters to render.
 * @param numv The array of number characters to render.
 */
void render_nums(SDL_Surface *target, SDL_Rect target_rect, unsigned char numc, unsigned char *numv)
{
	const unsigned char gap = 1;

	SDL_Rect rects[numc];
	unsigned char total_width = -gap;
	for (unsigned char i = 0; i < numc; i++)
	{
		SDL_Rect rect = HUD.rects[numv[i]];
		rects[i] = rect;
		total_width += rect.w + gap;
		// Align as if 1 was same width as other numbers to keep the HUD readable
		if (numv[i] == 1)
		{
			total_width += 4;
		}
	}
	unsigned char pad = (target_rect.w - total_width) * Settings.hud.alignment;

	target_rect.x += pad;
	target_rect.w -= pad;
	for (unsigned char i = 0; i < numc; i++)
	{
		SDL_Rect rect = rects[i];
		SDL_BlitSurface(HUD.surface, &rect, target, &target_rect);
		target_rect.x += rect.w + gap;
		target_rect.w -= rect.w + gap;
	}
}

/**
 * Format and render the given time to the given surface.
 * @param target The surface to render to.
 * @param rect The area of the surface in which to render.
 * @param time_ms The time to render in milliseconds.
 */
void render_time(SDL_Surface *target, SDL_Rect rect, unsigned int time_ms)
{
	int m = time_ms/60000;
	int s = (time_ms/1000) % 60;
	int ms = time_ms % 1000;

	if (m > 99)
	{
		m = 99;
		s = 99;
		ms = 999;
	}

	unsigned char numv[9];
	numv[0] = m / 10;
	numv[1] = m % 10;
	numv[2] = 10; //:
	numv[3] = s / 10;
	numv[4] = s % 10;
	numv[5] = 11; //.
	numv[6] = ms / 100;
	numv[7] = (ms % 100) / 10;
	numv[8] = ms % 10;

	unsigned char cut = 0;
	if (!numv[0])
	{
		cut = 1;
		if (!numv[1])
		{
			cut = 3;
			if (!numv[3])
			{
				cut = 4;
			}
		}
	}

	render_nums(target, rect, 9 - cut, &numv[cut]);
}

/**
 * Render the given number to the given surface.
 * @param target The surface to render to.
 * @param rect The area of the surface in which to render.
 * @param distance The number to render.
 */
void render_distance(SDL_Surface *target, SDL_Rect rect, unsigned int distance)
{
	if (distance > 99999999)
	{
		distance = 99999999;
	}

	unsigned char numc = 8;
	unsigned char numv[numc];

	unsigned char leading = 0;
	for (int i = 0; i < numc; i++)
	{
		unsigned char num = (distance % (int) pow(10,i+1)) / pow(10,i);
		numv[numc - i - 1] = num;
		if (num)
		{
			leading = 0;
		}
		else
		{
			leading++;
		}
	}
	// Show zero at the start
	if (leading == numc)
	{
		leading--;
	}

	numc -= leading;
	render_nums(target, rect, numc, &numv[leading]);
}

/**
 * Render the given time and distance to the given surface.
 * @param target The surface to render to.
 * @param time_ms The time to render in milliseconds.
 * @param distance The distance to render.
 */
void render_time_and_distance(SDL_Surface *target, unsigned int time_ms, unsigned int distance)
{
	if (Settings.flags.hide_counters)
	{
		return;
	}
	SDL_Rect time_rect = {0, 0, target->w, 11};
	SDL_Rect distance_rect = {0, 12, target->w, 11};

	SDL_FillRect(target, NULL, 0);

	render_time(target, time_rect, time_ms);
	render_distance(target, distance_rect, distance);
}

/**
 * Load the number graphics.
 */
void init_hud()
{
	if (Settings.flags.hide_counters)
	{
		return;
	}

	char nums_path[Settings.paths.data_dir_len + sizeof "nums.bmp"];
	snprintf(nums_path, sizeof nums_path, "%s%s", Settings.paths.data_dir, "nums.bmp");

	HUD.surface = SDL_LoadBMP(nums_path);
	if (!HUD.surface)
	{
		report_SDL_error("Loading data/nums.bmp");
		//return RET_SDL_ERR;
	}

	//                    0  1  2  3  4  5  6  7  8  9  :  .
	const int widths[] = {7, 3, 7, 7, 7, 7, 7, 7, 7, 7, 3, 3};

	int x = 0;
	for (int i = 0; i < 12; i++)
	{
		SDL_Rect rect = {x, 0, widths[i], 11};
		x += rect.w;
		HUD.rects[i] = rect;
	}
}

/**
 * Free the memory used by the number graphics.
 */
void free_hud()
{
	if (Settings.flags.hide_counters)
	{
		return;
	}

	SDL_FreeSurface(HUD.surface);
}
