/* Copyright © 2020 Linus Vanas <linus@vanas.fi>
 * SPDX-License-Identifier: MIT
 */
#include "common.h"

struct settings Settings =
{
	.paths = {.data_dir_len = 0, .config_file_len = 0},
	.window = {SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 480, 480},
	.tunnel =
	{
		.length = 200, .sectors = 10, .section_length = 1.0,
		.color_transition_length = {12, 60}
	},
	.ships =
	{
		.amount = 2, .depth = 1, .sector_offset = 2,
	},
	.difficulty =
	{
		.speed = 0.000075, .uncarved_safe_chance = 0.5, .carvers = 2, .transition_length = 8,
		.increase = {.speed = 0, .uncarved_safe_chance = -0.25, .carvers = -1, .transition_length = 0}
	},
	.hud = {.sector = 2, .depth = 3, .alignment = 0.95},
	.tick_time = {16, 24}
};

char try_set_uchar(char *setting_name, unsigned char *setting, char *setting_str,  char *value_str)
{
	if (strcmp(setting_name, setting_str))
	{
		return 0;
	}
	unsigned char value;
	if (sscanf(value_str, "%hhud", &value))
	{
		*setting = value;
		return 1;
	}
	fprintf(stderr, "Invalid %s: %s\n", setting_str, value_str);
	return 1;
}

char try_set_float(char *setting_name, float *setting, char *setting_str,  char *value_str)
{
	if (strcmp(setting_name, setting_str))
	{
		return 0;
	}
	float value;
	if (sscanf(value_str, "%f", &value))
	{
		*setting = value;
		return 1;
	}
	fprintf(stderr, "Invalid %s: %s\n", setting_str, value_str);
	return 1;
}

char try_set_short(char *setting_name, short *setting, char *setting_str,  char *value_str)
{
	if (strcmp(setting_name, setting_str))
	{
		return 0;
	}
	short value;
	if (sscanf(value_str, "%hd", &value))
	{
		*setting = value;
		return 1;
	}
	fprintf(stderr, "Invalid %s: %s\n", setting_str, value_str);
	return 1;
}

void set_setting(char *setting, char *value)
{
	if (!strcmp("seed", setting))
	{
		unsigned int seed;
		if (sscanf(value, "%ud", &seed))
		{
			Settings.game.seed = seed;
			return;
		}
		fprintf(stderr, "Invalid seed: %s\n", value);
		return;
	}

	if (try_set_uchar("hud-sector", &(Settings.hud.sector), setting, value)) return;
	if (try_set_uchar("hud-depth", &(Settings.hud.depth), setting, value)) return;
	if (try_set_float("hud-alignment", &(Settings.hud.alignment), setting, value)) return;

	if (try_set_uchar("min-tick-time", &(Settings.tick_time.min), setting, value)) return;
	if (try_set_uchar("max-tick-time", &(Settings.tick_time.max), setting, value)) return;

	if (try_set_float("initial-speed", &(Settings.difficulty.speed), setting, value)) return;
	if (try_set_float("initial-safe-chance", &(Settings.difficulty.uncarved_safe_chance), setting, value)) return;
	if (try_set_uchar("initial-safe-paths", &(Settings.difficulty.carvers), setting, value)) return;
	if (try_set_uchar("initial-safe-transition", &(Settings.difficulty.transition_length), setting, value)) return;

	if (try_set_float("death-speed-increase", &(Settings.difficulty.increase.speed), setting, value)) return;
	if (try_set_float("death-safe-chance-increase", &(Settings.difficulty.increase.uncarved_safe_chance), setting, value)) return;
	if (try_set_short("death-safe-paths-increase", &(Settings.difficulty.increase.carvers), setting, value)) return;
	if (try_set_short("death-safe-transition-increase", &(Settings.difficulty.increase.transition_length), setting, value)) return;

	if (try_set_uchar("ships", &(Settings.ships.amount), setting, value)) return;
	if (try_set_uchar("ship-depth", &(Settings.ships.depth), setting, value)) return;
	if (try_set_uchar("ship-sector-offset", &(Settings.ships.sector_offset), setting, value)) return;

	if (try_set_uchar("tunnel-sectors", &(Settings.tunnel.sectors), setting, value)) return;
	if (try_set_uchar("tunnel-length", &(Settings.tunnel.length), setting, value)) return;
	if (try_set_float("tunnel-segment-length", &(Settings.tunnel.section_length), setting, value)) return;

	if (try_set_uchar("min-color-transition-length", &(Settings.tunnel.color_transition_length.min), setting, value)) return;
	if (try_set_uchar("max-color-transition-length", &(Settings.tunnel.color_transition_length.max), setting, value)) return;

	fprintf(stderr, "Unrecognized setting: %s\n", setting);
}
