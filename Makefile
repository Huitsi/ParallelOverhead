#Copyright © 2020-2025 Linus Vanas <linus@vanas.fi>
#SPDX-License-Identifier: MIT

prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/games
datarootdir = $(prefix)/share
datadir = $(datarootdir)/games
appsdir = $(prefix)/share/applications
icondir = $(prefix)/share/icons/hicolor
metainfodir = $(prefix)/share/metainfo
mandir = $(prefix)/share/man
man6dir = $(mandir)/man6
man6ext = .6

destbindir = $(DESTDIR)$(bindir)
destdatadir = $(DESTDIR)$(datadir)
destappsdir = $(DESTDIR)$(appsdir)
desticondir = $(DESTDIR)$(icondir)
destmetainfodir = $(DESTDIR)$(metainfodir)
destman6dir = $(DESTDIR)$(man6dir)

CFLAGS = -Wall -O3
LDLIBS = -lSDL2 -lGLESv2 -lm

C = $(wildcard src/*.c)
O = $(patsubst src/%.c, build/%.o, $C)
D = $(patsubst %.o, %.d, $O)

LMMS = lmms
SFXR = sfxr-qt
HELP2MAN = help2man

version = $(shell git describe)

build_bin = build/parallel_overhead$(binext)
build_data = build/move.wav build/death.wav build/fast_pulse.wav
static_data = $(wildcard data/*.bmp) $(wildcard data/*.glsl)
build_man = build/parallel_overhead$(man6ext)

install_bin = $(destbindir)/parallel_overhead$(binext)
install_built_data = $(addprefix $(destdatadir)/parallel_overhead/, $(notdir $(build_data)))
install_static_data = $(addprefix $(destdatadir)/parallel_overhead/, $(notdir $(static_data)))
install_data = $(install_built_data) $(install_static_data)
install_man = $(destman6dir)/parallel_overhead$(man6ext)

install_app = $(destappsdir)/net.huitsi.ParallelOverhead.desktop
install_icon = $(desticondir)/48x48/apps/net.huitsi.ParallelOverhead.png $(desticondir)/scalable/apps/net.huitsi.ParallelOverhead.svg
install_metainfo = $(destmetainfodir)/net.huitsi.ParallelOverhead.metainfo.xml

install_files = $(install_bin) $(install_data) $(install_icon) $(install_metainfo) $(install_app) $(install_man)

.PHONY: all clean install uninstall assets

all: $(build_bin) $(build_man) $(build_data)

clean:
	$(RM) -r build

install: $(install_files)

uninstall:
	$(RM) $(install_app)
	$(RM) $(install_icon)
	$(RM) $(install_metainfo)
	$(RM) $(install_man)
	$(RM) $(destbindir)/parallel_overhead
	$(RM) -r $(destdatadir)/parallel_overhead

assets: $(build_data)

$(install_bin): $(build_bin)
	install -D $^ $@

$(install_static_data): $(destdatadir)/parallel_overhead/%: data/%
	install -m644 -D $^ $@

$(install_built_data): $(destdatadir)/parallel_overhead/%: build/%
	install -m644 -D $^ $@

$(desticondir)/48x48/apps/net.huitsi.ParallelOverhead.png: app/net.huitsi.ParallelOverhead.png
	install -m644 -D $^ $@

$(desticondir)/scalable/apps/net.huitsi.ParallelOverhead.svg: app/net.huitsi.ParallelOverhead.svg
	install -m644 -D $^ $@

$(install_metainfo): app/net.huitsi.ParallelOverhead.metainfo.xml
	install -m644 -D $^ $@

$(install_app): app/net.huitsi.ParallelOverhead.desktop
	desktop-file-install --dir=$(@D) $^

$(install_man): $(build_man)
	install -m644 -D $^ $@

build/config.h: | build
	echo '#define VERSION "'$(version)\" > $@

build/%.d: src/%.c | build/config.h
	$(CC) $(CPPFLAGS) -MM -MT build/$*.o $< -MF $@

build/%.o: src/%.c build/%.d
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(build_bin): $O | build
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

build/fast_pulse.wav: data-src/fast_pulse.mmp | build
	$(LMMS) render $^ $(LMMSFLAGS) -o $@

build/%.wav: data-src/%.sfxj | build
	$(SFXR) --export $^ $(SFXRFLAGS) -o $@

$(build_man): $(build_bin)
	$(HELP2MAN) $^ -s 6 -N -n "Endless runner game" -o $@

build:
	mkdir build

include $D
