#Copyright © 2020-2023 Linus Vanas <linus@vanas.fi>
#SPDX-License-Identifier: MIT

prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/games
datarootdir = $(prefix)/share
datadir = $(datarootdir)/games
appsdir = $(prefix)/share/applications
icondir = $(prefix)/share/icons/hicolor
metainfodir = $(prefix)/share/metainfo
mandir = $(prefix)/share/man
man6dir = $(mandir)/man6
man6ext = .6

destbindir = $(DESTDIR)$(bindir)
destdatadir = $(DESTDIR)$(datadir)
destappsdir = $(DESTDIR)$(appsdir)
desticondir = $(DESTDIR)$(icondir)
destmetainfodir = $(DESTDIR)$(metainfodir)
destman6dir = $(DESTDIR)$(man6dir)

CFLAGS = -Wall -O3

C = $(wildcard src/*.c)
O = $(patsubst src/%.c, build/%.o, $C)
D = $(patsubst %.o, %.d, $O)

LMMS = lmms
SFXR = sfxr-qt
HELP2MAN = help2man

data = $(notdir $(wildcard data/*.bmp) $(wildcard data/*.glsl))
build_data = move.wav death.wav fast_pulse.wav

install_bin = $(destbindir)/parallel_overhead
install_data = $(addprefix $(destdatadir)/parallel_overhead/, $(data))
install_built_data = $(addprefix $(destdatadir)/parallel_overhead/, $(build_data))

install_app = $(destappsdir)/net.huitsi.ParallelOverhead.desktop
install_icon = $(desticondir)/48x48/apps/net.huitsi.ParallelOverhead.png $(desticondir)/scalable/apps/net.huitsi.ParallelOverhead.svg
install_metainfo = $(destmetainfodir)/net.huitsi.ParallelOverhead.metainfo.xml
install_man = $(destman6dir)/parallel_overhead$(man6ext)

install_files = $(install_bin) $(install_data) $(install_built_data) $(install_icon) $(install_metainfo) $(install_app) $(install_man)

.PHONY: all clean install uninstall assets

all: build/parallel_overhead build/parallel_overhead.6 assets

clean:
	$(RM) -r build

install: $(install_files)

uninstall:
	$(RM) $(install_app)
	$(RM) $(install_icon)
	$(RM) $(install_metainfo)
	$(RM) $(install_man)
	$(RM) $(destbindir)/parallel_overhead
	$(RM) -r $(destdatadir)/parallel_overhead

assets: $(addprefix build/, $(build_data))

$(install_bin): build/parallel_overhead
	install -D $^ $@

$(install_data): $(destdatadir)/parallel_overhead/%: data/%
	install -m644 -D $^ $@

$(install_built_data): $(destdatadir)/parallel_overhead/%: build/%
	install -m644 -D $^ $@

$(desticondir)/48x48/apps/net.huitsi.ParallelOverhead.png: app/net.huitsi.ParallelOverhead.png
	install -m644 -D $^ $@

$(desticondir)/scalable/apps/net.huitsi.ParallelOverhead.svg: app/net.huitsi.ParallelOverhead.svg
	install -m644 -D $^ $@

$(install_metainfo): app/net.huitsi.ParallelOverhead.metainfo.xml
	install -m644 -D $^ $@

$(install_app): app/net.huitsi.ParallelOverhead.desktop
	desktop-file-install --dir=$(@D) $^

$(install_man): build/parallel_overhead.6
	install -m644 -D $^ $@

build/config.h: | build
	echo '#define VERSION "'$$(git describe)\" > $@

build/%.d: src/%.c | build/config.h
	$(CC) $(CPPFLAGS) -MM -MT build/$*.o $< -MF $@

build/%.o: src/%.c build/%.d
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

build/parallel_overhead: $O | build
	$(CC) $(LDFLAGS) $^ -lSDL2 -lGLESv2 -lm -o $@

build/fast_pulse.wav: data-src/fast_pulse.mmp | build
	$(LMMS) render $^ $(LMMSFLAGS) -o $@

build/%.wav: data-src/%.sfxj | build
	$(SFXR) --export $^ $(SFXRFLAGS) -o $@

build/parallel_overhead.6: build/parallel_overhead
	$(HELP2MAN) ./build/parallel_overhead -s 6 -N -n "Endless runner game" -o $@

build:
	mkdir build

include $D
