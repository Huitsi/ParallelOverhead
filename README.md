<!--
Copyright © 2020-2023 Linus Vanas <linus@vanas.fi>
SPDX-License-Identifier: MIT
-->
# Parallel Overhead

![Screenshot](screenshot.png)

**WARNING: This game is visually intensive and has rapidly changing colors.**

Parallel Overhead is a colorful endless runner game where you take control of
the ships Truth and Beauty on a groundbreaking trip through hyperspace. A
stable hyperspace tunnel has finally been achieved with the two ships
supporting it on opposite walls of the tunnel. Well, almost stable...
Keep the ships from falling through the cracks!

Parallel Overhead features
[Fast Pulse by oglsdl](https://opengameart.org/content/fast-pulse)
as its soundtrack.

## Links

* [Homepage](https://huitsi.net/ParallelOverhead/)
* [Project on Codeberg](https://codeberg.org/Huitsi/ParallelOverhead)
* [Releases on Codeberg](https://codeberg.org/Huitsi/ParallelOverhead/releases)

## Running the game

Parallel Overhead requires SDL2 and GLES 2.0 support. The game executable is
(by default) called `parallel_overhead`. It has a few optional command line
options, run it with the `--help`-option to find out more.

## Controls

The game starts paused to allow resizing the window and adjusting music and
sound effect volumes in the OS mixer. Closing the window will exit the game.

### Keyboard

* Space or enter to pause or unpause
* Left arrow key to move the ships left (clockwise rotation)
* Right arrow key to move the ships right (anticlockwise rotation)
* Backspace to restart
* Escape to exit

### Touch or mouse

* Tap/click in the middle to to pause or unpause
* Tap/click in the left quarter to move the ships left (clockwise rotation)
* Tap/click in the right quarter to move the ships right (anticlockwise rotation)

## Building from source

These instructions are for Debian 12 ("bookworm"). Adapt them as necessary for
your system and preferences. To build and run Parallel Overhead, run the
following commands:

1. `# apt install build-essential desktop-file-utils git help2man libsdl2-dev lmms sfxr-qt`
2. `$ git clone https://codeberg.org/Huitsi/ParallelOverhead.git`
3. `$ cd ParallelOverhead`
4. `$ make`
5. `$ make install prefix=local`
6. `$ ./parallel_overhead`

Note that only the first step requires root privileges, but Parallel Overhead
won't be installed system-wide. To do that, drop the `prefix=local` from step 5
and run it as root. Step 6 will then become just `$ parallel_overhead`.
